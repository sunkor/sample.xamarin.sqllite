using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sample.SqlLite
{
    class SQLiteDBManager
    {
        private string dbPath;

        public SQLiteDBManager(string dbPath)
        {
            this.dbPath = dbPath;
        }

        public string CreateDatabase()
        {
            try
            {
                var connection = new SQLiteAsyncConnection(dbPath);
                {
                    connection.CreateTableAsync<Person>();
                    return "Database created";
                }
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public async Task<string> InsertUpdateData(Person data)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbPath);
                var existingData = await db.FindAsync<Person>(x => x.ID == data.ID);
                int result = 0;
                if (existingData == null)
                {
                    result = await db.InsertAsync(data);
                }
                else
                {
                    result = await db.UpdateAsync(data);
                }
                return "Single data file inserted or updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public async Task<int> DeleteData(Person data)
        {
            int result;
            try
            {
                var db = new SQLiteAsyncConnection(dbPath);
                result = await db.DeleteAsync(data);// != 0
                //return "Single data file deleted";
            }
            catch (SQLiteException ex)
            {
                //return ex.Message;
                result = -1;
            }
            return result;
        }

        public List<Person> GetPersons()
        {
            var personsList = new List<Person>();
            var db = new SQLiteConnection(dbPath);
            var enumerator = db.Table<Person>().GetEnumerator();
            while (enumerator.MoveNext())
            {
                personsList.Add(enumerator.Current);
            }
            return personsList;
        }

        public async Task<int> FindNumberRecords()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbPath);
                // this counts all records in the database, it can be slow depending on the size of the database
                var count = await db.ExecuteScalarAsync<int>("SELECT Count(*) FROM Person");

                // for a non-parameterless query
                // var count = db.ExecuteScalar<int>("SELECT Count(*) FROM Person WHERE FirstName="Amy");

                return count;
            }
            catch (SQLiteException ex)
            {
                return -1;
            }
        }
    }
}