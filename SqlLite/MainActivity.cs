﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Sample.SqlLite
{
    [Activity(Label = "SqlLite", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private const string PERSON_DIALOG_FRAG_TAG = "PersonDialogFragment";

        private Lazy<string> DBPath = new Lazy<string>(() => Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PersonDatabase.db"));
        private SQLiteDBManager _dbManager = null;

        private ListView lvPersons = null;
        private PersonArrayAdapter adapter = null;
        private List<Person> persons = null;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            lvPersons = FindViewById<ListView>(Resource.Id.lvPersons);

            _dbManager = new SQLiteDBManager(DBPath.Value);
            _dbManager.CreateDatabase();

            Button btnAddPerson = FindViewById<Button>(Resource.Id.btnAddPerson);
            btnAddPerson.Click += (sender, e) =>
            {
                //AddPerson();
                LaunchPersonDialog(null);
            };

            //Button btnDeletePerson = FindViewById<Button>(Resource.Id.btnDeletePerson);
            //btnDeletePerson.Click += (sender, e) =>
            //{
            //    //AddPerson();
            //    LaunchPersonDialog();
            //};

            //lvPersons.ItemLongClick += LvPersons_ItemClick;
            lvPersons.ItemClick += LvPersons_ItemClick;

            //adapter = new ArrayAdapter<Person>(this, Android.Resource.Layout.SimpleListItem1);
            adapter = new PersonArrayAdapter(this);
            lvPersons.Adapter = adapter;
            RefreshList();
        }

        private void LvPersons_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var item = persons[e.Position];
            LaunchPersonDialog(item);
            //lvPersons.Animation = Resource.Animation.wobble_item;
        }

        private void RefreshList()
        {
            persons = _dbManager.GetPersons();
            adapter.Clear();
            adapter.AddAll(persons);
            adapter.NotifyDataSetChanged();
        }

        private void LaunchPersonDialog(Person person)
        {
            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFrag = new PersonDialogFragment(person);
            dialogFrag.OnPersonChanged += async (sender, pea) =>
            {
                await AddPerson(pea.Person);
                RefreshList();
            };
            dialogFrag.OnPersonDeleted += async (sender, pea) =>
            {
                await DeletePerson(pea.Person);
                RefreshList();
            };
            dialogFrag.Show(transaction, PERSON_DIALOG_FRAG_TAG);
        }
        
        private async Task<string> AddPerson(Person person)
        {
            return await _dbManager.InsertUpdateData(person);
        }

        private async Task DeletePerson(Person person)
        {
            await _dbManager.DeleteData(person);
        }
    }
}

