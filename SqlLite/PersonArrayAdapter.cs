﻿using Android.Content;
using Android.Widget;
using System.Collections.Generic;
using Java.Lang;
using Android.Views;
using Android.App;

namespace Sample.SqlLite
{
    public class PersonArrayAdapter : ArrayAdapter<Person>
    {
        private Activity activity;
        private List<Person> persons = new List<Person>();

        public PersonArrayAdapter(Activity activity) : base(activity, -1)
        {
            this.activity = activity;
        }

        public override int Count
        {
            get { return persons.Count; }
        }
        
        public override void Clear()
        {
            this.persons.Clear();
        }

        public override long GetItemId(int position)
        {
            return persons[position].ID;
        }

        public void AddAll(List<Person> persons)
        {
            this.persons = persons;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.PersonListItem, parent, false);

            var person = persons[position];

            TextView tvName = view.FindViewById<TextView>(Resource.Id.tvName);
            tvName.Text = string.Format("ID {0} - {1}, {2}", person.ID, person.LastName, person.FirstName);

            return view;
        }
    }
}