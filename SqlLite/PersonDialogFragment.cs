using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Sample.SqlLite
{
    class PersonEventArgs : EventArgs
    {
        public Person Person;
    }

    class PersonDialogFragment : DialogFragment
    {
        private Person person;

        private EditText etFirstName;
        private EditText etLastName;
        private Button btnSubmit;
        private Button btnDelete;

        public event EventHandler<PersonEventArgs> OnPersonChanged;
        public event EventHandler<PersonEventArgs> OnPersonDeleted;

        public PersonDialogFragment(Person person)
        {
            this.person = person;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.PersonDialog, container);

            etFirstName = view.FindViewById<EditText>(Resource.Id.etFirstName);
            etLastName = view.FindViewById<EditText>(Resource.Id.etLastName);
            btnSubmit = view.FindViewById<Button>(Resource.Id.btnSubmit);
            btnDelete = view.FindViewById<Button>(Resource.Id.btnDelete);

            if (person != null)
            {
                etFirstName.Text = person.FirstName;
                etLastName.Text = person.LastName;
                btnDelete.Visibility = ViewStates.Visible;
            }

            btnSubmit.Click += (sender, ea) =>
            {
                var eventArgs = new PersonEventArgs()
                {
                    Person = BuildPersonDetails()
                };
                OnPersonChanged.Invoke(this, eventArgs);
                this.Dismiss();
            };

            btnDelete.Click += (sender, ea) =>
            {
                var builder = new AlertDialog.Builder(this.Activity);
                builder.SetMessage("Do you want to delete this record ?");
                builder.SetPositiveButton("Yes", (s, e) =>
                {
                    var eventArgs = new PersonEventArgs()
                    {
                        Person = BuildPersonDetails()
                    };
                    OnPersonDeleted.Invoke(this, eventArgs);
                    this.Dismiss();
                });
                builder.SetNegativeButton("No", (s, e) => { /* do something on Cancel click */ });
                builder.Create().Show();
            };

            return view;
        }

        private Person BuildPersonDetails()
        {
            if (person == null)
            {
                this.person = new Person();
            }
            person.FirstName = etFirstName.Text;
            person.LastName = etLastName.Text;
            return person;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation;
        }
    }
}